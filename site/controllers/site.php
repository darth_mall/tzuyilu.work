<?php

return function ($page, $site) {
	$pageTitle = $page->isHomePage()
		? $site->title()->html()
		: $page->title()->html() . ' | ' . $site->title()->html();

	return compact('pageTitle');
};
