		</main>

		<footer>
			<p>
				&copy;
				<?= (new DateTimeImmutable('now'))->format('Y') ?> |
				<?= $site->author()->html() ?>
		</footer>
	</body>
</html>
