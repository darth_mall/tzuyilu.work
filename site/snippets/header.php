<html lang=en>
	<head>
		<meta charset=UTF-8>
		<meta name=viewport content="width=device-width, initial-scale=1.0">
		<meta http-equiv=X-UA-Compatible content="ie=edge">
		<meta name=color-scheme content="light dark">

		<title><?= $pageTitle ?></title>

		<style>@layer reset, base, layout</style>
		<?= css('assets/css/site.css') ?>
	</head>
	<body>
		<header>
			<a href="<?= url('/') ?>"><?= $site->title() ?></a>
			<nav>
				<a href="<?= url('/resume/') ?>">Resume</a>
			</nav>
		</header>
		<main class="flow">
