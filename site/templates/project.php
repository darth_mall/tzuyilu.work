<?php snippet('header') ?>

<article>
	<h1><?= $page->title() ?></h1>
	<?= $page->text()->toBlocks() ?>
</article>

<?php snippet('footer') ?>
