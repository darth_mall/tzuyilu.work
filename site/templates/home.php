<?php snippet('header') ?>

<h1>Work</h1>

<div>
	<?php foreach ($kirby->collection('projects') as $project): ?>
	<article>
		<h2>
			<a href="<?= $project->url() ?>"><?= $project->title()->html() ?></a>
		</h2>
	</article>
	<?php endforeach ?>
</div>

<?php snippet('footer') ?>
